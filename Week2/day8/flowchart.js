let patientRegistered = Math.random();
let queueNumber = Math.round(Math.random() * 10);
let needFollowup = Math.random();
let needMedication = Math.random();

// Start function
function start() {
  patientArrives();
}

// Patient arrives
function patientArrives() {
  console.log("Patient arrives");
  isPatientRegistered();
}

// Is the patient registered
function isPatientRegistered() {
  console.log("Checking if patient already registered...");
  if (patientRegistered < 0.5) {
    // If 'Yes', take queue number
    console.log("Patient is already registered.");
    takeQueueNumber();
  } else {
    // If 'No', register patient
    console.log("Patient is not registered.");
    console.log("Registering patient...");
    setTimeout(() => {
      // Patient is registered, take queue number
      console.log("Patient registered.");
      takeQueueNumber();
    }, Math.round(Math.random() * 3000));
  }
}

// Take queue number
function takeQueueNumber() {
  if (isPatientRegistered) {
    console.log("Taking queue number...");
    console.log();
    waitingForQueueCalled();
  }
}

// Is queue number called?
function waitingForQueueCalled() {
  console.log("Waiting in lobby.");
  let i = 1;
  function myLoop() {
    setTimeout(function () {
      // If 'No', wait in lobby
      console.log(`Queue number ${i} is called...`);
      i++;
      if (i <= queueNumber) {
        myLoop();
      } else {
        // If 'Yes', doctor checkup
        console.log(
          `Your queue number ${queueNumber} is called. Going to the doctor's ward.`
        );
        doctorCheckup();
      }
    }, Math.round(Math.random() * 3000));
  }

  myLoop();
}

// Doctor checkup
function doctorCheckup() {
  console.log("Doctor checkup...");
  patientneedFollowup();
}

// Does the patient need followup check?
function patientneedFollowup() {
  if (needFollowup < 0.3) {
    // If 'Yes', arrange next appointment
    console.log(
      "A dangerous disease is detected! Patient needs more appointment..."
    );
    setTimeout(() => {
      console.log("Next appointment is arranged.");
      patientNeedMedication();
    }, 2000);
  } else {
    // If 'No', proceed to patientNeedMedication
    console.log(
      "Doctor determined there is no need for additional appointment."
    );
    patientNeedMedication();
  }
}

// Does the patient need medication?
function patientNeedMedication() {
  if (needMedication > 0.1) {
    // If 'Yes', take medicine from pharmacy then proceed to makePayment
    console.log("Patient needs medication.");
    setTimeout(() => {
      console.log("Take medicine from pharmacy.");
      makePayment();
    }, Math.round(Math.random() * 3000));
  } else {
    // If 'No', proceed to makePayment
    console.log("Doctor determined there is no need for medication.");
    makePayment();
  }
}

// Make payment
function makePayment() {
  console.log("Making payment...");
  setTimeout(() => {
    finish();
  }, Math.round(Math.random() * 3000));
}

// Finish function
function finish() {
  console.log("Finish.");
}

// Execute on runtime
start();
