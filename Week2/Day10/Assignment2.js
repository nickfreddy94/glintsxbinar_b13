const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function menu() {
  console.log("PATIENT STAT");
  console.log("============");
  console.log("1. Postive");
  console.log("2. Suspect");
  console.log("3. Negative");
  console.log("4. Exit");

  rl.question("Please choice Stat Menu:", (pilihan) => {
    switch (eval(pilihan)) {
      case 1:
        console.log(
          `Patients who are indicated as Positive COVID-19 : John, Nike, Reebox`
        );
        menu();
        break;
      case 2:
        console.log(
          `Patients who are indicated as Suspect COVID-19 : Mike, Adidas, Fladeo `
        );
        menu();
        break;
      case 3:
        console.log(
          `Patients who are Negative COVID-19 : Bata, Boss, Brodo, Wakai`
        );
        menu();
        break;
      case 4:
        rl.close();
        break;
      default:
        console.log(`Option must be 1 to 4!\n`);
        menu();
        break;
    }
  });
}

module.exports = { menu };
