const express = require("express");
const router = express.Router();

const handphoneController = require("../controller/handphoneController");

router.get("/", handphoneController.getAllHandphone);

router.get("/id/:id", handphoneController.getHandphoneById);
router.get("/RAM/:RAM", handphoneController.gethandphoneByRAM);

router.post("/", handphoneController.addNewhandphone);

router.put("/id/:id", handphoneController.updatephone);

router.delete("/id/:id", handphoneController.deletephone);

module.exports = router;
