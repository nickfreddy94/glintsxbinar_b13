const express = require("express");

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

//routes
const handphoneroutes = require("./routes/handphoneroute");
// const example = require("./routes/example");

app.use("/handphones", handphoneroutes);
// app.use("/example", example);
app.listen(port, () => {
  console.log(`server running on port ${port}`);
});
