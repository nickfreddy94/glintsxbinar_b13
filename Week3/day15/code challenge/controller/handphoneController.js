let handphones = require("../models/handphoneseries.json");

class HandphoneController {
  getAllHandphone(req, res) {
    try {
      res.status(200).json({
        data: handphones,
      });
    } catch (err) {
      res.status(500).json({
        message: err.message,
      });
    }
  }

  getHandphoneById(req, res) {
    try {
      let data = handphones.filter(
        (handphone) => req.params.id == handphone.id
      );
      if (data.length > 0) {
        res.status(200).json({
          data: data,
        });
      } else {
        res.status(418).json({
          message: `handphone with id ${req.params.id} not found!`,
        });
      }
    } catch (err) {
      res.status(500).json({
        message: err.message,
      });
    }
  }

  gethandphoneByRAM(req, res) {
    try {
      let data = handphones.filter(
        (handphone) => req.params.RAM == handphone.RAM
      );
      if (data.length > 0) {
        res.status(200).json({
          data: data,
        });
      } else {
        res.status(418).json({
          message: `handphone with RAM like ${req.params.RAM} not found!`,
        });
      }
    } catch (err) {
      res.status(500).json({
        message: err.message,
      });
    }
  }

  addNewhandphone(req, res) {
    try {
      handphones.push(req.body);
      res.status(200).json({
        data: req.body,
      });
    } catch (err) {
      res.status(500).json({
        message: err.message,
      });
    }
  }

  updatephone = async (req, res) => {
    try {
      const handphone = await handphones.find(
        (data) => data.id == req.params.id
      );

      if (handphone) {
        const update = await handphones.find((data) => {
          if (data.id == req.params.id) {
            data.name = req.body.name;
          }
        });
        res.status(200).json({
          message: "updated succesfully",
          data: handphone,
        });
      } else {
        res.status(418).json({
          message: "No data found",
        });
      }
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  };

  deletephone(req, res) {
    try {
      let FIX = handphones.filter((handphone) => handphone.id == req.params.id);
      handphones = handphones.filter(
        (handphone) => handphone.id != req.params.id
      );
      if (FIX.length > 0) {
        res.status(200).json({
          data: FIX,
        });
      } else {
        res.status(204).send();
      }
    } catch (err) {
      res.status(500).json({
        message: err.message,
      });
    }
  }
}

module.exports = new HandphoneController();
