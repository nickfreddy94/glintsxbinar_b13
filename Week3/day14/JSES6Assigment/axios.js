const axios = require("axios");

const url1 = "https://jsonplaceholder.typicode.com/posts/1";
const url2 = "https://jsonplaceholder.typicode.com/posts/2";
const url3 = "https://jsonplaceholder.typicode.com/posts/3";
const url4 = "https://jsonplaceholder.typicode.com/posts/4";
const url5 = "https://jsonplaceholder.typicode.com/posts/5";
const url6 = "https://jsonplaceholder.typicode.com/posts/6";
const url7 = "https://jsonplaceholder.typicode.com/posts/7";
const url8 = "https://jsonplaceholder.typicode.com/posts/8";
const url9 = "https://jsonplaceholder.typicode.com/posts/9";
const url10 = "https://jsonplaceholder.typicode.com/posts/10";

async function fetch1() {
  try {
    axios
      .get(url1)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}
async function fetch2() {
  try {
    axios
      .get(url2)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}
async function fetch3() {
  try {
    axios
      .get(url3)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}

async function fetch4() {
  try {
    axios
      .get(url4)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}

async function fetch5() {
  try {
    axios
      .get(url5)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}
async function fetch6() {
  try {
    axios
      .get(url6)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}
async function fetch7() {
  try {
    axios
      .get(url7)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}

async function fetch8() {
  try {
    axios
      .get(url8)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}async function fetch9() {
  try {
    axios
      .get(url9)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}
async function fetch10() {
  try {
    axios
      .get(url10)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}

fetch1();
fetch2();
fetch3();
fetch4();

fetch5();
fetch6();
fetch7();
fetch8();

fetch9();
fetch10();


