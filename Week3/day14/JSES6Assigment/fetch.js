const fetch = require("node-fetch");

const url1 = "https://jsonplaceholder.typicode.com/posts/1";
const url2 = "https://jsonplaceholder.typicode.com/posts/2";
const url3 = "https://jsonplaceholder.typicode.com/posts/3";
const url4 = "https://jsonplaceholder.typicode.com/posts/4";
const url5 = "https://jsonplaceholder.typicode.com/posts/5";
async function fetchAPI() {
  try {
    let response = await fetch(url1);
    let data = await response.json();
    console.log(data);

    response = await fetch(url2);
    data = await response.json();
    console.log(data);

    response = await fetch(url3);
    data = await response.json();
    console.log(data);

    response = await fetch(url4);
    data = await response.json();
    console.log(data);

    response = await fetch(url5);
    data = await response.json();
    console.log(data);
  } catch (error) {
    console.error("Getting an error, exiting!");
  }
}
fetchAPI();
