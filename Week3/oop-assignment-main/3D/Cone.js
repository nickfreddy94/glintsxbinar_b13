const ThreeDimension = require("./ThreeDimension");

class Cone extends ThreeDimension {
  constructor(height, radius) {
    super("Cone");
    this.height = height;
    this.radius = radius;
  }

  calculateVolume(name) {
    super.calculateVolume();
    //1/3 x π x r x r x t
    const formula = (1 / 3) * Math.PI * this.radius * this.radius * this.height;
    return `${name}, ${formula}`;
  }

  calculateArea(name) {
    super.calculateArea();
    const formula = 2 * Math.PI * this.radius * (this.radius + this.height);
    return `${name}, ${formula} `;
  }
}

module.exports = Cone;
